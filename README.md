# CPU Emulation module for Ghidra

This module seeks to allow easy interactive use of Ghidra's PCode emulator to help with reverse engineering.

It provides a `gdb`-like interface to _debug_ the emulated code.

## Initial Setup

Ghidra uses Jython which only supports the, now deprecated, Python 2.
This extension uses [ghidra_bridge](https://github.com/justfoxing/ghidra_bridge), to access Python 3 functionality.

### Dependencies

We recommend the use of a python virtual environment to manage dependencies.

Install the [virtualenv](https://pypi.org/project/virtualenv/) python module.

Then run `make install_bridge`. This will create a virtual environment in `venv/`, install all dependencies inside it and copy the `ghidra_bridge` extension to `$HOME/ghidra_scripts`.


## Usage

First, open the program you wish to analyze in Ghidra's Code Browser view.

Then, [start the bridge server](https://github.com/justfoxing/ghidra_bridge#start-server).

Launch the debugger with `python src/app.py` and you're ready to go.

### Referencing operands and locations

Much like `gdb`, this module uses special prefix characters to denote the type of some operands.

Registers use a `$` sign. _e.g._ `$rax`

Memory addresses use the `*`: _e.g._ `*0x401234`

### Available commands

 - Run: `r [start_address]`
   - Run program until next breakpoint. If no `start_address` is provided, starts from the current program counter position.
 - Add breakpoint: `b [address]`
   - Add a breakpoint at target address. If no `address` is provided, uses the current program counter value.
 - Delete breakpoint: `d <breakpoint>`
   - Unsets a breakpoint.
   - `breakpoint` can be an address or index (as displayed by the _Breakpoint info_ command)
 - Continue: `c`
   - Run the program until next breakpoint
 - Single step: `s`
   - Execute a single instruction
 - Set: `set <target>=<value>`
   - Set the value of `target` to `value`.
     - Target must be a register or memory address.
	 - Value can be a literal, a register or a memory address
 - Print: `p <target>`
   - Output the value of `target`
	 - Target can be a literal, a register or a memory address
 - Assembly listing: `l [address] [num_lines]`
 - Examine: `x `
   - **Unimplemented**
 - Quit: `q `
   - **Unimplemented**
 - Stop: `t `
   - **Unimplemented**
 - Breakpoint info: `i`
   - Outputs all currently set breakpoints with their address and index.
