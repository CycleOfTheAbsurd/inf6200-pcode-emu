#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import bridge
import pemulator
import readline
from registerutils import address_factory
from sys import stderr
from collections import deque
from jfx_bridge.bridge import BridgeException

LISTING_LINES = 2
pemu = pemulator.PEmulator(bridge.currentProgram)

def run(start_addr=None, *args):
    if start_addr is not None:
        addr = pemu.val_to_addr(start_addr[1:])
        if addr is None:
            return
        pemu.pc(addr.getOffset())
    elif pemu.pc() == 0:
        pemu.pc(bridge.getState().getCurrentAddress().getOffset())
    pemu.run()

def add_break(addr=None):
    if addr is None:
        addr = bridge.getState().getCurrentAddress()
    else:
        addr = pemu.val_to_addr(addr[1:])
        if addr is None:
            return
    pemu.add_breakpoint(addr)

def delete_break(target):
    if target[0] == "*":
        addr = pemu.val_to_addr(target[1:])
        if addr is not None:
            if not pemu.remove_breakpoint(addr):
                print("No breakpoint present at address '0x{}'".format(addr), file=stderr)
    else:
        try:
            idx = int(target, 0)
        except ValueError:
            print("Invalid id '{}'".format(target), file=stderr)
        else:
            if not pemu.remove_breakpoint(idx, True):
                print("No breakpoint with id '{}'".format(idx), file=stderr)

def info_break(*args):
    print("Breakpoints:")
    for i, a in pemu.list_breakpoints():
        print("{:>2}: 0x{}".format(i, a))

def cont(*args):
    pemu.run()

def step(*args):
    if pemu.pc() == 0:
        pemu.pc(bridge.getState().getCurrentAddress().getOffset())
    pemu.single_step()

def printr(target):
    val, err = pemu.get_value(target)
    if err is None:
        if type(val) == int:
            val = "0x{:x}".format(val)
        print(val)
    else:
        print(err, file=stderr)

def setr(target, value, *args):
    val, err = pemu.get_value(value)
    if err is not None:
        print(err, file=stderr)
        return
    if target[0] == "*": #address
        try:
            size = int(args[0], 0)
        except IndexError:
            pemu.set_mem(target, val)
        except ValueError:
            print("Invalid value for size: \"{}\", using default size".format(args[0]), file=stderr)
            pemu.set_mem(target, val)
        else:
            pemu.set_mem(target, val, size)
    elif target[0] == "$": #register
        try:
            reg = target[1:]
            pemu.set_reg(reg, val)
        except BridgeException as e:
            err = "No such register '{}'".format(reg)
    else:
        print("Cannot set a literal '{}'".format(target), file=stderr)

def listing(addr_val=None, lines=LISTING_LINES):
    lines = int(lines)
    if addr_val is None:
        addr_val = pemu.pc()
    if addr_val[0] == "*":
        addr_val = addr_val[1:]
    addr = pemu.val_to_addr(addr_val)
    instruction = bridge.getInstructionAt(addr)
    instructions = deque()
    ins = instruction
    for i in range(lines):
        ins = bridge.getInstructionBefore(ins)
        if ins is not None:
            instructions.appendleft(ins)
    ins = instruction
    if ins is not None:
        instructions.append(ins)
    for i in range(lines):
        ins = bridge.getInstructionAfter(ins)
        if ins is not None:
            instructions.append(ins)
    for i in instructions:
        _print_asm(i)

def examine(*args):
    pass

def quit(*args):
    pass

def stop(*args):
    pass

COMMANDS = {
    "r": run,
    "b": add_break,
    "d": delete_break,
    "c": cont,
    "s": step,
    "set": setr,
    "p": printr,
    "l": listing,
    "x": examine,
    "q": quit,
    "t": stop,
    "i": info_break,
}

def mainloop():
    while True:
        prompt = "> "
        if pemu.pc() != 0:
            prompt = "[0x{:08x}]> ".format(pemu.pc())
        try:
            cmd = input(prompt).split(" ")
        except (EOFError, KeyboardInterrupt):
            return
        if len(cmd[0]) == 0:
            cmd = readline.get_history_item(readline.get_current_history_length()).split(" ")
        try:
            COMMANDS[cmd[0]](*cmd[1:])
        except KeyError:
            print('Undefined command "{}".'.format(cmd[0]), file=stderr)
        except TypeError as e:
            print(e, file=stderr)

def _print_asm(instruction):
    print("0x{}: {}".format(instruction.getAddress(), instruction))

if __name__ == "__main__":
    mainloop()
