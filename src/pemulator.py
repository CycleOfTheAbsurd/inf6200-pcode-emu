# -*- encoding: utf-8 -*-

import bridge
import registerutils
emulator = bridge.gb.remote_import("ghidra.app.emulator")
lang = bridge.gb.remote_import("ghidra.program.model.lang")
task = bridge.gb.remote_import("ghidra.util.task")

class PEmulator():
    def __init__(self, program):
        self.emuHelper = emulator.EmulatorHelper(program)
        self.monitor = task.ConsoleTaskMonitor()
        self.addressFactory = program.getAddressFactory()
        self._display_registers = registerutils.get_display_registers(self.emuHelper.getLanguage())
        self._breakpoints = []

    def pc(self,addr=None):
        pc = registerutils.get_pc_register(self.emuHelper.getLanguage()).getName()
        if addr is None:
            return self.get_reg(pc)
        else:
            self.set_reg(pc, addr)

    def val_to_addr(self, value):
        return self.addressFactory.getDefaultAddressSpace().getAddress(value)

    def get_reg(self, reg_name):
        return registerutils.get_register_value(self.emuHelper, reg_name)

    def set_reg(self, reg_name, value):
        registerutils.set_register_value(self.emuHelper, reg_name, value)

    def set_mem(self, addr, value, size=registerutils.BITSIZE//8):
        address = registerutils.to_address(addr[1:])
        if address is not None:
            val_bytes = value.to_bytes(size, registerutils.ENDIANNESS)
            self.emuHelper.writeMemory(address, val_bytes)

    def get_value(self, target):
        return registerutils.get_value(self.emuHelper, target)

    def single_step(self):
        ok = self.emuHelper.step(self.monitor)
        if not ok:
            print(self.emuHelper.getLastError())

    def add_breakpoint(self, address):
        try:
            index = self._breakpoints.index(address)
            return index
        except ValueError:
            self.emuHelper.setBreakpoint(address)
            self._breakpoints.append(address)
            return len(self._breakpoints) - 1

    def remove_breakpoint(self, target, by_index=False):
        if by_index:
            if len(self._breakpoints) <= target or self._breakpoints[target] is None:
                return False
            else:
                self.emuHelper.clearBreakpoint(self._breakpoints[target])
                self._breakpoints[target] = None
        else:
            for i,a in enumerate(self._breakpoints):
                if a == target:
                    self.emuHelper.clearBreakpoint(a)
                    self._breakpoints[i] = None
                    break
            else:
                return False
        return True

    def list_breakpoints(self):
        for i, a in enumerate(self._breakpoints):
            if a is not None:
                yield i,a

    def run(self):
        self.emuHelper.run(self.monitor)
        if self.emuHelper.getExecutionAddress() not in self._breakpoints:
            print("Execution reached end of program")

