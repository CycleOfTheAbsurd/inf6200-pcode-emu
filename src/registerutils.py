# -*- encoding: utf-8 -*-

# List of registers to be displayed for specific architectures
# Adapted from https://github.com/hugsy/gef
from jfx_bridge.bridge import BridgeException
from bridge import currentProgram
from sys import stderr
import struct

BITSIZE = currentProgram.getLanguage().getLanguageDescription().getSize()
ENDIANNESS_STRUCT = ">" if currentProgram.getLanguage().isBigEndian() else "<"
ENDIANNESS = "big" if currentProgram.getLanguage().isBigEndian() else "little"
address_factory = currentProgram.getAddressFactory()

display_reg = {
    "RISCV": ["$zero", "$ra", "$sp", "$gp", "$tp", "$t0", "$t1",
             "$t2", "$fp", "$s1", "$a0", "$a1", "$a2", "$a3",
             "$a4", "$a5", "$a6", "$a7", "$s2", "$s3", "$s4",
             "$s5", "$s6", "$s7", "$s8", "$s9", "$s10", "$s11",
             "$t3", "$t4", "$t5", "$t6",],
    "ARM":  ["$r0", "$r1", "$r2", "$r3", "$r4", "$r5", "$r6",
             "$r7", "$r8", "$r9", "$r10", "$r11", "$r12", "$sp",
             "$lr", "$pc", "$cpsr",],
    "AARCH64":  ["$x0", "$x1", "$x2", "$x3", "$x4", "$x5", "$x6", "$x7",
            "$x8", "$x9", "$x10", "$x11", "$x12", "$x13", "$x14", "$x15",
            "$x16", "$x17", "$x18", "$x19", "$x20", "$x21", "$x22", "$x23",
            "$x24", "$x25", "$x26", "$x27", "$x28", "$x29", "$x30", "$sp",
            "$pc", "$cpsr", "$fpsr", "$fpcr",],
    "x86":  {
                32: ["$eax", "$ebx", "$ecx", "$edx", "$esp", "$ebp", "$esi", "$edi",
                    "$eip", "$eflags", "$cs", "$ss", "$ds", "$es", "$fs", "$gs",],
                64: ["$rax", "$rbx", "$rcx", "$rdx", "$rsp", "$rbp", "$rsi", "$rdi", "$rip",
                    "$r8", "$r9", "$r10", "$r11", "$r12", "$r13", "$r14", "$r15", "$eflags",
                    "$cs", "$ss", "$ds", "$es", "$fs", "$gs",],
            },
    "PowerPC": ["$r0", "$r1", "$r2", "$r3", "$r4", "$r5", "$r6", "$r7",
        "$r8", "$r9", "$r10", "$r11", "$r12", "$r13", "$r14", "$r15",
        "$r16", "$r17", "$r18", "$r19", "$r20", "$r21", "$r22", "$r23",
        "$r24", "$r25", "$r26", "$r27", "$r28", "$r29", "$r30", "$r31",
        "$pc", "$msr", "$cr", "$lr", "$ctr", "$xer", "$trap",],
    "Sparc": {
                32: ["$g0", "$g1", "$g2", "$g3", "$g4", "$g5", "$g6", "$g7",
                    "$o0", "$o1", "$o2", "$o3", "$o4", "$o5", "$o7",
                    "$l0", "$l1", "$l2", "$l3", "$l4", "$l5", "$l6", "$l7",
                    "$i0", "$i1", "$i2", "$i3", "$i4", "$i5", "$i7",
                    "$pc", "$npc","$sp ","$fp ","$psr",],
                64: ["$g0", "$g1", "$g2", "$g3", "$g4", "$g5", "$g6", "$g7",
                    "$o0", "$o1", "$o2", "$o3", "$o4", "$o5", "$o7",
                    "$l0", "$l1", "$l2", "$l3", "$l4", "$l5", "$l6", "$l7",
                    "$i0", "$i1", "$i2", "$i3", "$i4", "$i5", "$i7",
                    "$pc", "$npc", "$sp", "$fp", "$state", ],
            },
    "MIPS": {
                32: ["$zero", "$at", "$v0", "$v1", "$a0", "$a1", "$a2", "$a3",
                    "$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7",
                    "$s0", "$s1", "$s2", "$s3", "$s4", "$s5", "$s6", "$s7",
                    "$t8", "$t9", "$k0", "$k1", "$s8", "$pc", "$sp", "$hi",
                    "$lo", "$fir", "$ra", "$gp", ],
            }
}

def to_address(addr):
    try:
        address = int(addr, 0)
        address = address_factory.getDefaultAddressSpace().getAddress(address)
    except ValueError:
        print("Invalid address '{}'".format(addr), file=stderr)
        return None
    except BridgeException:
        print("Address out of bounds '{}'".format(addr), file=stderr)
        return None
    return address

def get_display_registers(language):
    proc = str(language.getProcessor())
    size = language.getLanguageDescription().getSize()
    try: #Check if we have a display config for the register
        display = display_reg[proc]
    except:
        display = [str(r) for r in lang.getRegisters if r.isBaseRegister() and r.getGroup() is None]
    else: #Check if config depends on processor size
        if type(display) == dict:
            display = display[size]
    return display

def get_pc_register(language):
    return language.getProgramCounter()
def get_register_value(emuHelper, reg):
    return emuHelper.readRegister(reg)

def set_register_value(emuHelper, reg, value):
    emuHelper.writeRegister(reg, value)

def get_value(emuHelper, target):
    err = None
    val = None
    if target[0] == "*": #address
        addr = to_address(target[1:])
        if addr is not None:
            val = [v if v >=0 else 256+v for v in emuHelper.readMemory(addr, BITSIZE//8)]
            val = " ".join(["0x{:02x}".format(v) for v in val])
            #val = [struct.pack(ENDIANNESS_STRUCT+"c", v.to_bytes(1, byteorder=ENDIANNESS, signed=False)) for v in val]
        else:
            err = "Unable to read from address"
    elif target[0] == "$": #register
        try:
            reg = target[1:]
            val = get_register_value(emuHelper, reg)
        except BridgeException:
            err = "No such register '{}'".format(reg)
    else: #literal
        try:
            val = int(target, 0)
        except ValueError as e:
            err = e
    return val, err
