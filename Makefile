default: deps

venv:
	python -m virtualenv venv

deps: venv
	sh -c ". venv/bin/activate && python -m pip install -r requirements.txt"

install_bridge: deps
	sh -c ". venv/bin/activate && python -m python -m ghidra_bridge.install_server ${HOME}/ghidra_scripts"

